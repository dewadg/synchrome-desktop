module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Echelons', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      echelonTypeId: {
        type: Sequelize.STRING
      },
      supervisorId: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Echelons')
  }
}
