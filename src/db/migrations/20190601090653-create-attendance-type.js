module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('AttendanceTypes', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.BOOLEAN
      },
      tppPaid: {
        type: Sequelize.BOOLEAN
      },
      mealAllowancePaid: {
        type: Sequelize.BOOLEAN
      },
      manualInput: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('AttendanceTypes')
  }
}
