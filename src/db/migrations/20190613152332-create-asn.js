module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Asns', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      agencyId: {
        type: Sequelize.STRING
      },
      rankId: {
        type: Sequelize.STRING
      },
      echelonId: {
        type: Sequelize.STRING
      },
      tppId: {
        type: Sequelize.INTEGER
      },
      workshiftId: {
        type: Sequelize.INTEGER
      },
      calendarId: {
        type: Sequelize.INTEGER
      },
      pin: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      phone: {
        type: Sequelize.STRING
      },
      address: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Asns')
  }
}
