const moment = require('moment')

module.exports = {
  up: (queryInterface, Sequelize) => {
    const now = moment().format('YYYY-MM-DD')

    return queryInterface.bulkInsert('Configs', [
      {
        key: 'agencyId',
        value: '-',
        createdAt: now,
        updatedAt: now
      },
      {
        key: 'isInitialized',
        value: 'false',
        createdAt: now,
        updatedAt: now
      },
      {
        key: 'lastSyncAt',
        value: '-',
        createdAt: now,
        updatedAt: now
      }
    ], {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Configs', null, {})
  }
}
