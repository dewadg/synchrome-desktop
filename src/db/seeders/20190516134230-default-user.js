const bcrypt = require('bcryptjs')
const moment = require('moment')

module.exports = {
  up: (queryInterface, Sequelize) => {
    const salt = bcrypt.genSaltSync(10)

    return queryInterface.bulkInsert('Users', [
      {
        id: 1,
        name: 'admin',
        password: bcrypt.hashSync('admin', salt),
        createdAt: moment().format('YYYY-MM-DD'),
        updatedAt: moment().format('YYYY-MM-DD')
      }
    ], {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {})
  }
}
