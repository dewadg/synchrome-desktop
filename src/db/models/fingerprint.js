module.exports = (sequelize, DataTypes) => {
  const Fingerprint = sequelize.define('Fingerprint', {
    asnId: DataTypes.STRING,
    idx: DataTypes.STRING,
    algVer: DataTypes.INTEGER,
    template: DataTypes.TEXT
  }, {})
  Fingerprint.associate = function (models) {
    // associations can be defined here
  }
  return Fingerprint
}
