/* eslint-disable no-path-concat */
const Sequelize = require('sequelize')

const config = {
  username: process.env.ELECTRON_WEBPACK_APP_DB_USER,
  password: process.env.ELECTRON_WEBPACK_APP_DB_PASSWORD,
  database: process.env.ELECTRON_WEBPACK_APP_DB_DATABASE,
  host: process.env.ELECTRON_WEBPACK_APP_DB_HOST,
  dialect: process.env.ELECTRON_WEBPACK_APP_DB_DIALECT
}

const sequelize = new Sequelize(config.database, config.username, config.password, config)

const db = {
  User: require('./user')(sequelize, Sequelize.DataTypes),
  Config: require('./config')(sequelize, Sequelize.DataTypes),
  AttendanceType: require('./attendanceType')(sequelize, Sequelize.DataTypes),
  EchelonType: require('./echelonType')(sequelize, Sequelize.DataTypes),
  Echelon: require('./echelon')(sequelize, Sequelize.DataTypes),
  Rank: require('./rank')(sequelize, Sequelize.DataTypes),
  Agency: require('./agency')(sequelize, Sequelize.DataTypes),
  Calendar: require('./calendar')(sequelize, Sequelize.DataTypes),
  CalendarEvent: require('./calendarEvent')(sequelize, Sequelize.DataTypes),
  Workshift: require('./workshift')(sequelize, Sequelize.DataTypes),
  WorkshiftDetails: require('./workshiftDetails')(sequelize, Sequelize.DataTypes),
  Tpp: require('./tpp')(sequelize, Sequelize.DataTypes),
  Asn: require('./asn')(sequelize, Sequelize.DataTypes),
  Fingerprint: require('./fingerprint')(sequelize, Sequelize.DataTypes),
  Attendance: require('./attendance')(sequelize, Sequelize.DataTypes)
}

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

export default db
