module.exports = (sequelize, DataTypes) => {
  const Attendance = sequelize.define('Attendance', {
    asnId: DataTypes.STRING,
    date: DataTypes.DATEONLY,
    checkIn: DataTypes.TIME,
    checkOut: DataTypes.TIME,
    delay: DataTypes.INTEGER,
    attendanceTypeId: DataTypes.STRING,
    info: DataTypes.TEXT
  }, {})
  Attendance.associate = function (models) {
    Attendance.belongsTo(models.AttendanceType, {
      as: 'type',
      foreignKey: 'attendanceTypeId'
    })

    Attendance.belongsTo(models.Asn, {
      as: 'asn',
      foreignKey: 'asnId'
    })
  }
  return Attendance
}
