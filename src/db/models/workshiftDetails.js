module.exports = (sequelize, DataTypes) => {
  const WorkshiftDetails = sequelize.define('WorkshiftDetails', {
    workshiftId: DataTypes.INTEGER,
    index: DataTypes.INTEGER,
    checkIn: DataTypes.TIME,
    checkOut: DataTypes.TIME,
    active: DataTypes.BOOLEAN
  }, {})
  WorkshiftDetails.associate = function (models) {
    // associations can be defined here
  }
  return WorkshiftDetails
}
