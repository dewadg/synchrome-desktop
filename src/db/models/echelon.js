module.exports = (sequelize, DataTypes) => {
  const Echelon = sequelize.define('Echelon', {
    name: DataTypes.STRING,
    echelonTypeId: DataTypes.STRING,
    supervisorId: DataTypes.STRING
  }, {})
  Echelon.associate = function (models) {
    Echelon.belongsTo(models.EchelonType, {
      as: 'type',
      foreignKey: 'echelonTypeId'
    })

    Echelon.belongsTo(Echelon, {
      as: 'supervisor',
      foreignKey: 'supervisorId'
    })
  }
  return Echelon
}
