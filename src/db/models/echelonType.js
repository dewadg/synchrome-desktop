module.exports = (sequelize, DataTypes) => {
  const EchelonType = sequelize.define('EchelonType', {
    name: DataTypes.STRING
  }, {})
  EchelonType.associate = function (models) {
    // associations can be defined here
  }
  return EchelonType
}
