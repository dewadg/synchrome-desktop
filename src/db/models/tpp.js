module.exports = (sequelize, DataTypes) => {
  const Tpp = sequelize.define('Tpp', {
    name: DataTypes.STRING,
    value: DataTypes.DECIMAL
  }, {})
  Tpp.associate = function (models) {
    // associations can be defined here
  }
  return Tpp
}
