module.exports = (sequelize, DataTypes) => {
  const CalendarEvent = sequelize.define('CalendarEvent', {
    title: DataTypes.STRING,
    calendarId: DataTypes.INTEGER,
    start: DataTypes.DATE,
    end: DataTypes.DATE,
    attendanceTypeId: DataTypes.STRING
  }, {})
  CalendarEvent.associate = function (models) {
    CalendarEvent.belongsTo(models.AttendanceType, {
      as: 'attendanceType',
      foreignKey: 'attendanceTypeId'
    })
  }
  return CalendarEvent
}
