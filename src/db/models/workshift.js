module.exports = (sequelize, DataTypes) => {
  const Workshift = sequelize.define('Workshift', {
    name: DataTypes.STRING
  }, {})
  Workshift.associate = function (models) {
    Workshift.hasMany(models.WorkshiftDetails, {
      as: 'details',
      foreignKey: 'workshiftId'
    })
  }
  return Workshift
}
