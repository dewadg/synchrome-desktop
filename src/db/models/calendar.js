module.exports = (sequelize, DataTypes) => {
  const Calendar = sequelize.define('Calendar', {
    name: DataTypes.STRING,
    start: DataTypes.DATE,
    end: DataTypes.DATE,
    published: DataTypes.BOOLEAN
  }, {})
  Calendar.associate = function (models) {
    Calendar.hasMany(models.CalendarEvent, {
      as: 'events',
      foreignKey: 'calendarId'
    })
  }
  return Calendar
}
