module.exports = (sequelize, DataTypes) => {
  const Asn = sequelize.define('Asn', {
    agencyId: DataTypes.STRING,
    rankId: DataTypes.STRING,
    echelonId: DataTypes.STRING,
    tppId: DataTypes.INTEGER,
    workshiftId: DataTypes.INTEGER,
    calendarId: DataTypes.INTEGER,
    pin: DataTypes.STRING,
    name: DataTypes.STRING,
    phone: DataTypes.STRING,
    address: DataTypes.TEXT
  }, {})
  Asn.associate = function (models) {
    Asn.belongsTo(models.Agency, {
      as: 'agency',
      foreignKey: 'agencyId'
    })

    Asn.belongsTo(models.Rank, {
      as: 'rank',
      foreignKey: 'rankId'
    })

    Asn.belongsTo(models.Echelon, {
      as: 'echelon',
      foreignKey: 'echelonId'
    })

    Asn.belongsTo(models.Tpp, {
      as: 'tpp',
      foreignKey: 'tppId'
    })

    Asn.belongsTo(models.Workshift, {
      as: 'workshift',
      foreignKey: 'workshiftId'
    })

    Asn.belongsTo(models.Calendar, {
      as: 'calendar',
      foreignKey: 'calendarId'
    })

    Asn.hasMany(models.Fingerprint, {
      as: 'fingerprints',
      foreignKey: 'asnId'
    })

    Asn.hasMany(models.Attendance, {
      as: 'attendances',
      foreignKey: 'asnId'
    })
  }
  return Asn
}
