module.exports = (sequelize, DataTypes) => {
  const AttendanceType = sequelize.define('AttendanceType', {
    name: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
    tppPaid: DataTypes.BOOLEAN,
    mealAllowancePaid: DataTypes.BOOLEAN,
    manualInput: DataTypes.BOOLEAN
  }, {})
  AttendanceType.associate = function (models) {
    // associations can be defined here
  }
  return AttendanceType
}
