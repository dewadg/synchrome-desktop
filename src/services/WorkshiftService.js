export default class WorkshiftService {
  constructor (db) {
    this._db = db
  }

  async get () {
    const { Workshift, WorkshiftDetails } = this._db

    try {
      const workshifts = await Workshift.findAll({
        include: [
          {
            model: WorkshiftDetails,
            as: 'details'
          }
        ]
      })

      return workshifts
    } catch (err) {
      throw err
    }
  }
}
