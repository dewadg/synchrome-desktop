import EasyLink from 'easylink-js'
import moment from 'moment'

const mapAttendanceTypeData = ({ dataValues }) => ({ ...dataValues })

const mapCalendarEventsData = events => events.map(({ dataValues }) => ({
  ...dataValues,
  attendanceType: dataValues.attendanceType
    ? mapAttendanceTypeData(dataValues.attendanceType)
    : null
}))

const mapCalendarData = ({ dataValues }) => ({
  ...dataValues,
  events: dataValues.events
    ? mapCalendarEventsData(dataValues.events)
    : []
})

const mapWorkshiftDetailsData = details => details.map(({ dataValues }) => ({
  ...dataValues
}))

const mapWorkshiftData = ({ dataValues }) => ({
  ...dataValues,
  details: dataValues.details
    ? mapWorkshiftDetailsData(dataValues.details)
    : []
})

const mapAsnData = ({ dataValues }) => ({
  ...dataValues,
  calendar: dataValues.calendar
    ? mapCalendarData(dataValues.calendar)
    : null,
  workshift: dataValues.workshift
    ? mapWorkshiftData(dataValues.workshift)
    : null
})

export default class AttendanceService {
  constructor (db, easyLinkConfig) {
    this._db = db
    this._easyLink = new EasyLink(easyLinkConfig)
  }

  async generate (date) {
    const { Asn, Attendance } = this._db
    const asns = await Asn.findAll()
    const attendancePromises = []

    try {
      for (let asn of asns) {
        const attendanceExists = await Attendance.findOne({
          where: {
            asnId: asn.id,
            date: moment(date).format('YYYY-MM-DD')
          }
        })

        if (attendanceExists) continue

        attendancePromises.push(Attendance.create({
          asnId: asn.id,
          date
        }))
      }

      await Promise.all(attendancePromises)
    } catch (err) {
      throw err
    }
  }

  async get (date) {
    const {
      Asn,
      Attendance,
      AttendanceType,
      Workshift,
      WorkshiftDetails,
      Calendar,
      CalendarEvent
    } = this._db

    try {
      const attendances = await Attendance.findAll({
        include: [
          {
            model: Asn,
            as: 'asn',
            include: [
              {
                model: Calendar,
                as: 'calendar',
                include: [
                  {
                    model: CalendarEvent,
                    as: 'events',
                    include: [
                      {
                        model: AttendanceType,
                        as: 'attendanceType'
                      }
                    ]
                  }
                ]
              },
              {
                model: Workshift,
                as: 'workshift',
                include: [
                  {
                    model: WorkshiftDetails,
                    as: 'details'
                  }
                ]
              }
            ]
          },
          {
            model: AttendanceType,
            as: 'type',
            required: false
          }
        ],
        where: {
          date: moment(date).format('YYYY-MM-DD')
        }
      })

      return attendances.map(({ dataValues }) => ({
        ...dataValues,
        asn: dataValues.asn
          ? mapAsnData(dataValues.asn)
          : null,
        type: dataValues.type
          ? mapAttendanceTypeData(dataValues.type)
          : null
      }))
    } catch (err) {
      throw err
    }
  }

  async fetchFromMachine (date) {
    try {
      const currentDate = moment(date).format('YYYY-MM-DD')
      const data = (await this._easyLink.getAllScanLogs())

      return data
        .filter((item) => {
          const scanLogDate = moment(item.ScanDate).format('YYYY-MM-DD')

          return scanLogDate === currentDate
        })
        .map(item => ({
          pin: item.PIN,
          date: moment(item.ScanDate)
        }))
    } catch (err) {
      throw err
    }
  }

  async process (date, data) {
    const { Attendance } = this._db
    const currentDate = moment(date).format('YYYY-MM-DD')

    const normalizedData = data.reduce((carry, item) => {
      carry[item.pin] = carry[item.pin]
        ? [...carry[item.pin], item]
        : [item]

      return carry
    }, {})

    const attendances = await this.get(currentDate)
    const processPromises = []

    for (let attendance of attendances) {
      const scanLogs = normalizedData[attendance.asn.pin]
      const payload = this.processAttendanceType(attendance.asn, scanLogs)

      processPromises.push(Attendance.update(payload, {
        where: {
          id: attendance.id
        }
      }))
    }

    await Promise.all(processPromises)
  }

  processAttendanceType (asn, scanLogs) {
    const now = moment()
    const todayShift = asn.workshift.details.find(item => item.index.toString() === now.format('E'))
    const shiftCheckIn = todayShift ? moment(`${now.format('YYYY-MM-DD')} ${todayShift.checkIn}`) : null
    const shiftCheckout = todayShift ? moment(`${now.format('YYYY-MM-DD')} ${todayShift.checkOut}`) : null
    const isTodayWorkday = todayShift ? todayShift.active : false

    // No scan log, over workshift checkout
    if (!scanLogs && now.isAfter(shiftCheckout)) {
      return {
        attendanceTypeId: 'A',
        delay: shiftCheckout.diff(shiftCheckIn, 'minutes')
      }
    }

    const firstScanLog = scanLogs[0]
    const secondScanLog = scanLogs[1] ? scanLogs[1] : null

    // Today is off
    if (!isTodayWorkday) {
      return {
        checkIn: null,
        checkOut: null
      }
    }

    const eventToday = asn.calendar.events.find((item) => {
      const start = moment(item.start)
      const end = item.end ? moment(item.end) : start.add(1, 'day')

      return now.isBetween(start, end)
    })

    // Normal working day
    if (!eventToday) {
      if (now.isAfter(shiftCheckout)) {
        return {
          checkIn: firstScanLog.date.format('HH:mm:ss'),
          checkOut: secondScanLog ? secondScanLog.date.format('HH:mm:ss') : null,
          attendanceTypeId: 'A',
          delay: shiftCheckout.diff(shiftCheckIn, 'minutes')
        }
      }

      const delay = firstScanLog.date.diff(shiftCheckIn, 'minutes')

      return {
        checkIn: firstScanLog.date.format('HH:mm:ss'),
        checkOut: secondScanLog ? secondScanLog.date.format('HH:mm:ss') : null,
        attendanceTypeId: secondScanLog ? 'H' : null,
        delay: delay > 0 ? delay : 0,
        info: delay > 0 ? `Terlambat ${delay} menit` : null
      }
    }

    // Today has special event
    switch (eventToday.attendanceType.id) {
      case 'L':
        return {
          checkIn: null,
          checkOut: null,
          attendanceTypeId: 'L',
          delay: 0,
          info: eventToday.title
        }
      default:
        return {
          checkIn: firstScanLog.date.format('HH:mm:ss'),
          checkOut: secondScanLog ? secondScanLog.date.format('HH:mm:ss') : null
        }
    }
  }
}
