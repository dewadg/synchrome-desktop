export default class EchelonService {
  constructor (db) {
    this._db = db
  }

  async get () {
    const { Echelon, EchelonType } = this._db

    try {
      const echelons = await Echelon.findAll({
        include: [
          {
            model: EchelonType,
            as: 'type'
          },
          {
            model: Echelon,
            as: 'supervisor'
          }
        ]
      })

      return echelons
    } catch (err) {
      throw err
    }
  }
}
