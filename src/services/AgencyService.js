export default class AgencyService {
  constructor (db) {
    this._db = db
  }

  async get () {
    const { Agency } = this._db

    try {
      const agencies = await Agency.findAll()

      return agencies
    } catch (err) {
      throw err
    }
  }
}
