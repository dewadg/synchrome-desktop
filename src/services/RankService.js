export default class RankService {
  constructor (db) {
    this._db = db
  }

  async get () {
    const { Rank } = this._db

    try {
      const ranks = await Rank.findAll()

      return ranks
    } catch (err) {
      throw err
    }
  }
}
