export default class AttendanceTypeService {
  constructor (db) {
    this._db = db
  }

  async get () {
    const { AttendanceType } = this._db

    try {
      const attendanceTypes = await AttendanceType.findAll()

      return attendanceTypes
    } catch (err) {
      throw err
    }
  }
}
