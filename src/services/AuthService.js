export default class AuthService {
  _db = null
  _bcrypt = null

  constructor (db, bcrypt) {
    this._db = db
    this._bcrypt = bcrypt
  }

  async authenticate (name, password) {
    try {
      const users = await this._db.User.findAll({
        where: {
          name
        },
        limit: 1
      })

      if (users.length === 0) {
        throw new Error('Pengguna tidak ditemukan')
      }

      const passwordCorrect = this._bcrypt.compareSync(password, users[0].password)

      if (!passwordCorrect) {
        throw new Error('Kata sandi salah')
      }

      return {
        id: users[0].id,
        name: users[0].name,
        createdAt: users[0].createdAt,
        updatedAt: users[0].updatedAt
      }
    } catch (err) {
      throw err
    }
  }
}
