import axios from 'axios'
import moment from 'moment'
import { SYNC_AUTH } from '@/constants/syncEndpoints'

export default class SyncService {
  constructor (syncModules, db, requestConfig) {
    this._syncModules = syncModules
    this._db = db
    this._requestConfig = requestConfig
  }

  async authenticate (name, password) {
    const url = `${this._requestConfig.baseURL}/${SYNC_AUTH}`

    try {
      const { data } = await axios.post(url, {
        name,
        password
      })

      this._axios = axios.create({
        ...this._requestConfig,
        auth: data
      })
    } catch (err) {
      if (err.response.status === 400) {
        throw new Error('Nama pengguna/kata sandi salah')
      }

      throw err
    }
  }

  async sync (syncModule) {
    if (!this._axios) {
      throw new Error('Authenticate sync module first')
    }

    try {
      syncModule.loading = true

      if (!this._syncModules[syncModule.model]) {
        console.info(`No sync module for ${syncModule.model}`)
      } else {
        await this._syncModules[syncModule.model](syncModule, this._axios, this._db)
      }

      syncModule.loading = false
      syncModule.success = true
    } catch (err) {
      console.error(err.message)

      syncModule.error = err

      throw err
    }
  }

  async updateLastSync () {
    const { Config } = this._db

    await Config.update({ value: 'true' }, {
      where: {
        key: 'isInitialized'
      }
    })

    await Config.update({ value: moment().format('YYYY-MM-DD HH:mm:ss') }, {
      where: {
        key: 'lastSyncAt'
      }
    })
  }

  async postReq (url, data) {
    if (!this._axios) {
      throw new Error('Authenticate sync module first')
    }

    try {
      const response = await this._axios.post(url, data)

      return response
    } catch (err) {
      throw err
    }
  }
}
