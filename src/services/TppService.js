export default class TppService {
  constructor (db) {
    this._db = db
  }

  async get () {
    const { Tpp } = this._db

    try {
      const tpps = await Tpp.findAll()

      return tpps
    } catch (err) {
      throw err
    }
  }
}
