export default class CalendarService {
  constructor (db) {
    this._db = db
  }

  async get () {
    const { Calendar, CalendarEvent } = this._db

    try {
      const calendars = await Calendar.findAll({
        include: [
          {
            model: CalendarEvent,
            as: 'events'
          }
        ]
      })

      return calendars
    } catch (err) {
      throw err
    }
  }
}
