import AttendanceType from './attendanceTypeSync'
import EchelonType from './echelonTypeSync'
import Echelon from './echelonSync'
import Rank from './rankSync'
import Agency from './agencySync'
import Calendar from './calendarSync'
import Workshift from './workshiftSync'
import Asn from './asnSync'
import Tpp from './tppSync'
import Attendance from './attendanceSync'

export default {
  AttendanceType,
  EchelonType,
  Echelon,
  Rank,
  Agency,
  Calendar,
  Workshift,
  Asn,
  Tpp,
  Attendance
}
