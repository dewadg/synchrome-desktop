import moment from 'moment'
import { removeDataNamespace } from '~/helpers/data'

export default async function (syncModule, axios, db) {
  const model = db[syncModule.model]
  if (!model) return

  try {
    const { data } = await axios.get(syncModule.target)
    const formattedData = removeDataNamespace(data.data)

    for (let item of formattedData) {
      console.info(`Processing ${syncModule.model} ${item.id}`)

      const foundData = await model.findByPk(item.id)
      const payload = {
        id: item.id,
        name: item.name,
        echelonTypeId: item.type.id,
        supervisorId: item.supervisor ? item.supervisor.id : null,
        createdAt: item.createdAt,
        updatedAt: item.updatedAt
      }

      if (!foundData) {
        await model.create(payload)
      } else {
        if (moment(foundData.updatedAt).isBefore(item.updatedAt)) {
          await model.update(payload, {
            where: {
              id: formattedData.id
            }
          })
        }
      }
    }
  } catch (err) {
    throw err
  }
}
