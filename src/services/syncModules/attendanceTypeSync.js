import moment from 'moment'
import { removeDataNamespace } from '~/helpers/data'

export default async function (syncModule, axios, db) {
  const model = db[syncModule.model]
  if (!model) return

  try {
    const { data } = await axios.get(syncModule.target)
    const formattedData = removeDataNamespace(data.data)

    for (let item of formattedData) {
      console.info(`Processing ${syncModule.model} ${item.id}`)

      const foundData = await model.findByPk(item.id)

      if (!foundData) {
        await model.create(item)
      } else {
        if (moment(foundData.updatedAt).isBefore(item.updatedAt)) {
          await model.update(item, {
            where: {
              id: formattedData.id
            }
          })
        }
      }
    }
  } catch (err) {
    throw err
  }
}
