import moment from 'moment'
import { removeDataNamespace } from '~/helpers/data'

export default async function (syncModule, axios, db) {
  const calendarModel = db[syncModule.model]
  if (!calendarModel) return

  try {
    const { data } = await axios.get(syncModule.target)
    const formattedData = removeDataNamespace(data.data)

    for (let item of formattedData) {
      console.info(`Processing ${syncModule.model} ${item.id}`)

      const calendarFoundData = await calendarModel.findByPk(item.id)
      const calendarPayload = {
        id: item.id,
        name: item.name,
        start: item.start,
        end: item.end,
        published: item.published
      }

      if (!calendarFoundData) {
        await calendarModel.create(calendarPayload)
      } else {
        if (moment(calendarFoundData.updatedAt).isBefore(item.updatedAt)) {
          await calendarModel.update(calendarPayload, {
            where: {
              id: item.id
            }
          })
        }
      }

      const eventModel = db.CalendarEvent
      if (!eventModel) return

      for (let eventData of item.events) {
        const eventFoundData = await eventModel.findByPk(eventData.id)
        const eventPayload = {
          id: eventData.id,
          calendarId: item.id,
          title: eventData.title,
          start: eventData.start,
          end: eventData.end,
          attendanceTypeId: eventData.attendanceType ? eventData.attendanceType.id : null
        }

        if (!eventFoundData) {
          await eventModel.create(eventPayload)
        } else {
          if (moment(eventFoundData.updatedAt).isBefore(eventData.updatedAt)) {
            await eventModel.update(eventPayload, {
              where: {
                id: eventData.id
              }
            })
          }
        }
      }
    }
  } catch (err) {
    throw err
  }
}
