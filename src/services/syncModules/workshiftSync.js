import moment from 'moment'
import { removeDataNamespace } from '~/helpers/data'

export default async function (syncModule, axios, db) {
  const workshiftModel = db[syncModule.model]
  if (!workshiftModel) return

  try {
    const { data } = await axios.get(syncModule.target)
    const formattedData = removeDataNamespace(data.data)

    for (let item of formattedData) {
      console.info(`Processing ${syncModule.model} ${item.id}`)

      const workshiftFoundData = await workshiftModel.findByPk(item.id)
      const workshiftPayload = {
        id: item.id,
        name: item.name
      }

      if (!workshiftFoundData) {
        await workshiftModel.create(workshiftPayload)
      } else {
        if (moment(workshiftFoundData.updatedAt).isBefore(item.updatedAt)) {
          await workshiftModel.update(workshiftPayload, {
            where: {
              id: item.id
            }
          })
        }
      }

      const detailsModel = db.WorkshiftDetails
      if (!detailsModel) return

      for (let detailsData of item.details) {
        const detailsFoundData = await detailsModel.findByPk(detailsData.id)
        const detailsPayload = {
          id: detailsData.id,
          workshiftId: item.id,
          index: detailsData.index,
          checkIn: detailsData.checkIn,
          checkOut: detailsData.checkOut,
          active: detailsData.active
        }

        if (!detailsFoundData) {
          await detailsModel.create(detailsPayload)
        } else {
          if (moment(detailsFoundData.updatedAt).isBefore(detailsData.updatedAt)) {
            await detailsModel.update(detailsPayload, {
              where: {
                id: detailsData.id
              }
            })
          }
        }
      }
    }
  } catch (err) {
    throw err
  }
}
