import moment from 'moment'

export default async function (syncModule, axios, db) {
  const model = db[syncModule.model]
  if (!model) return

  try {
    const start = moment().subtract(3, 'days').format('YYYY-MM-DD')
    const end = moment().add(1, 'days').format('YYYY-MM-DD')

    const data = await model.findAll({
      where: {
        date: {
          [db.Sequelize.Op.between]: [start, end]
        }
      }
    })

    const attendances = data
      .filter(item => item.attendanceTypeId)
      .map(({ dataValues }) => ({
        asnId: dataValues.asnId,
        date: dataValues.date,
        checkIn: dataValues.checkIn,
        checkOut: dataValues.checkOut,
        delay: dataValues.delay,
        attendanceTypeId: dataValues.attendanceTypeId,
        info: dataValues.info
      }))

    if (!attendances.length) return

    await axios.post(syncModule.target, { attendances })
  } catch (err) {
    throw err
  }
}
