import moment from 'moment'
import { removeDataNamespace } from '~/helpers/data'

export default async function (syncModule, axios, db) {
  const model = db[syncModule.model]
  if (!model) return

  try {
    const agencyIdConfig = await db.Config.findOne({
      where: {
        key: 'agencyId'
      },
      attributes: ['key', 'value']
    })

    if (!agencyIdConfig) return

    const url = syncModule.target.replace('{id}', agencyIdConfig.value)
    const { data } = await axios.get(url)
    const formattedData = removeDataNamespace(data.data)

    for (let item of formattedData) {
      console.info(`Processing ${syncModule.model} ${item.id}`)

      const foundData = await model.findByPk(item.id)
      const payload = {
        ...item,
        agencyId: item.agency
          ? item.agency.id
          : null,
        rankId: item.rank
          ? item.rank.id
          : null,
        echelonId: item.echelon
          ? item.echelon.id
          : null,
        tppId: item.tpp
          ? item.tpp.id
          : null,
        workshiftId: item.workshift
          ? item.workshift.id
          : null,
        calendarId: item.calendar
          ? item.calendar.id
          : null
      }

      if (!foundData) {
        await model.create(payload)
      } else {
        if (moment(foundData.updatedAt).isBefore(item.updatedAt)) {
          await model.update(payload, {
            where: {
              id: formattedData.id
            }
          })
        }
      }

      const fingerprintModel = db.Fingerprint
      if (!fingerprintModel) return

      for (let fingerprintData of item.fingerprints) {
        const fingerprintFoundData = await fingerprintModel.findByPk(fingerprintData.id)
        const fingerprintPayload = {
          id: fingerprintData.id,
          asnId: item.id,
          idx: fingerprintData.idx,
          algVer: fingerprintData.algVer,
          template: fingerprintData.template
        }

        if (!fingerprintFoundData) {
          await fingerprintModel.create(fingerprintPayload)
        } else {
          if (moment(fingerprintFoundData.updatedAt).isBefore(fingerprintData.updatedAt)) {
            await fingerprintModel.update(fingerprintPayload, {
              where: {
                id: fingerprintData.id
              }
            })
          }
        }
      }
    }
  } catch (err) {
    throw err
  }
}
