export default class ConfigService {
  _db = null

  constructor (db) {
    this._db = db
  }

  async find (key) {
    const config = await this._db.Config.findOne({
      where: {
        key
      },
      attributes: ['key', 'value']
    })

    if (!config) {
      throw new Error(`Konfigurasi '${key}' tidak ditemukan`)
    }

    return {
      key: config.key,
      value: config.value
    }
  }

  async set (key, value) {
    const config = await this._db.Config.findOne({
      where: {
        key
      },
      attributes: ['key', 'value']
    })

    if (!config) {
      await this._db.Config.create({
        key,
        value
      })
    } else {
      await this._db.Config.update({ value }, { where: { key } })
    }
  }
}
