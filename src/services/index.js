import bcrypt from 'bcryptjs'
import db from '~/db/models'

import AuthService from './AuthService'
import ConfigService from './ConfigService'
import SyncService from './SyncService'
import AttendanceTypeService from './AttendanceTypeService'
import AgencyService from './AgencyService'
import EchelonService from './EchelonService'
import RankService from './RankService'
import CalendarService from './CalendarService'
import WorkshiftService from './WorkshiftService'
import AsnService from './AsnService'
import FingerprintService from './FingerprintService'
import AttendanceService from './AttendanceService'

import axiosSyncConfig from '~/constants/axiosSyncConfig'
import easylinkConfig from '~/constants/easylinkConfig'

import syncModules from './syncModules'

export const authService = new AuthService(db, bcrypt)
export const configService = new ConfigService(db)
export const syncService = new SyncService(syncModules, db, axiosSyncConfig)
export const attendanceTypeService = new AttendanceTypeService(db)
export const agencyService = new AgencyService(db)
export const echelonService = new EchelonService(db)
export const rankService = new RankService(db)
export const calendarService = new CalendarService(db)
export const workshiftService = new WorkshiftService(db)
export const asnService = new AsnService(db)
export const fingerprintService = new FingerprintService(easylinkConfig, syncService)
export const attendanceService = new AttendanceService(db, easylinkConfig)
