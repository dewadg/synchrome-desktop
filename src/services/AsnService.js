export default class AsnService {
  constructor (db) {
    this._db = db
  }

  async get () {
    const {
      Asn,
      Agency,
      Rank,
      Echelon,
      Tpp,
      Workshift,
      Calendar,
      Fingerprint
    } = this._db

    try {
      const asns = await Asn.findAll({
        include: [
          {
            model: Agency,
            as: 'agency'
          },
          {
            model: Rank,
            as: 'rank'
          },
          {
            model: Echelon,
            as: 'echelon'
          },
          {
            model: Tpp,
            as: 'tpp'
          },
          {
            model: Workshift,
            as: 'workshift'
          },
          {
            model: Calendar,
            as: 'calendar'
          },
          {
            model: Fingerprint,
            as: 'fingerprints'
          }
        ]
      })

      return asns
    } catch (err) {
      throw err
    }
  }

  async find (id) {
    const {
      Asn,
      Agency,
      Rank,
      Echelon,
      Tpp,
      Workshift,
      Calendar,
      Fingerprint
    } = this._db

    try {
      const asn = await Asn.findByPk(id, {
        include: [
          {
            model: Agency,
            as: 'agency'
          },
          {
            model: Rank,
            as: 'rank'
          },
          {
            model: Echelon,
            as: 'echelon'
          },
          {
            model: Tpp,
            as: 'tpp'
          },
          {
            model: Workshift,
            as: 'workshift'
          },
          {
            model: Calendar,
            as: 'calendar'
          },
          {
            model: Fingerprint,
            as: 'fingerprints'
          }
        ]
      })

      return asn
    } catch (err) {
      throw err
    }
  }
}
