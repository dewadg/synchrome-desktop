import Vue from 'vue'
import Vuetify from 'vuetify'

import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
  theme: {
    primary: '#4E342E',
    accent: '#A1887F'
  }
})
