import Vue from 'vue'
import PageWrapper from '@/components/PageWrapper'
import UtilityCard from '@/components/UtilityCard'
import UtilityCardMenu from '@/components/UtilityCardMenu'
import UtilityCardSearchBar from '@/components/UtilityCardSearchBar'
import DatePicker from '@/components/DatePicker'

Vue.component('PageWrapper', PageWrapper)
Vue.component('UtilityCard', UtilityCard)
Vue.component('UtilityCardMenu', UtilityCardMenu)
Vue.component('UtilityCardSearchBar', UtilityCardSearchBar)
Vue.component('DatePicker', DatePicker)
