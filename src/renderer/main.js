import Vue from 'vue'
import './plugins/electron'
import './plugins/vue-router'
import './plugins/vuex'
import './plugins/vuetify'
import './plugins/vuelidate'
import './plugins/global-components'

import App from './App'
import router from './router'
import store from './store'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
