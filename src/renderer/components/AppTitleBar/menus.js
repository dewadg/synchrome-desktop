export default [
  // {
  //   to: { name: 'attendance_types.index' },
  //   text: 'Jenis Kehadiran'
  // },
  // {
  //   to: { name: 'echelon_types.index' },
  //   text: 'Jenis Eselon'
  // },
  {
    to: { name: 'agencies.index' },
    text: 'OPD'
  },
  {
    to: { name: 'echelons.index' },
    text: 'Eselon'
  },
  {
    to: { name: 'ranks.index' },
    text: 'Golongan'
  },
  {
    to: { name: 'asn.index' },
    text: 'ASN'
  },
  {
    to: { name: 'calendars.index' },
    text: 'Kalender'
  },
  {
    to: { name: 'workshifts.index' },
    text: 'Shift Kerja'
  },
  {
    to: { name: 'attendance.index' },
    text: 'Presensi'
  }
]
