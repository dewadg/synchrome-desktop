import { required } from 'vuelidate/lib/validators'

export default {
  form: {
    name: {
      required
    },
    password: {
      required
    }
  }
}
