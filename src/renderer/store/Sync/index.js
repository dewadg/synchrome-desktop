import state from './syncState'
import mutations from './syncMutations'
import actions from './syncActions'

export default {
  state,
  mutations,
  actions
}
