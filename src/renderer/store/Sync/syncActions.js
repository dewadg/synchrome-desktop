import * as types from './syncTypes'
import { syncService } from '~/services'

export default {
  async [types.SYNC_AUTHENTICATE] ({ commit }, payload) {
    commit(types.SYNC_AUTHENTICATE)

    try {
      await syncService.authenticate(payload.name, payload.password)

      commit(types.SYNC_AUTHENTICATE_SUCCESS)
    } catch (err) {
      commit(types.SYNC_AUTHENTICATE_ERROR, err)
    }
  },

  async [types.SYNC] ({ commit }, syncModule) {
    commit(types.SYNC)

    try {
      await syncService.sync(syncModule)

      commit(types.SYNC_SUCCESS)
    } catch (err) {
      commit(types.SYNC_ERROR, err)
    }
  },

  async [types.UPDATE_LAST_SYNC] ({ commit }) {
    commit(types.UPDATE_LAST_SYNC)

    try {
      await syncService.updateLastSync()

      commit(types.UPDATE_LAST_SYNC_SUCCESS)
    } catch (err) {
      commit(types.UPDATE_LAST_SYNC_ERROR, err)
    }
  }
}
