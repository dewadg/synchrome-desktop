export const SYNC_AUTHENTICATE = 'SYNC_AUTHENTICATE'
export const SYNC_AUTHENTICATE_SUCCESS = 'SYNC_AUTHENTICATE_SUCCESS'
export const SYNC_AUTHENTICATE_ERROR = 'SYNC_AUTHENTICATE_ERROR'

export const SYNC = 'SYNC'
export const SYNC_SUCCESS = 'SYNC_SUCCESS'
export const SYNC_ERROR = 'SYNC_ERROR'

export const UPDATE_LAST_SYNC = 'UPDATE_LAST_SYNC'
export const UPDATE_LAST_SYNC_SUCCESS = 'UPDATE_LAST_SYNC_SUCCESS'
export const UPDATE_LAST_SYNC_ERROR = 'UPDATE_LAST_SYNC_ERROR'
