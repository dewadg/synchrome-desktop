import * as types from './syncTypes'

export default {
  [types.SYNC_AUTHENTICATE] (state) {
    state.isAuthenticating = true
    state.error = null
  },

  [types.SYNC_AUTHENTICATE_SUCCESS] (state) {
    state.isAuthenticating = false
  },

  [types.SYNC_AUTHENTICATE_ERROR] (state, err) {
    state.isAuthenticating = false
    state.error = err
  },

  [types.SYNC] (state) {
    state.isSyncing = true
    state.error = null
  },

  [types.SYNC_SUCCESS] (state) {
    state.isSyncing = false
  },

  [types.SYNC_ERROR] (state, err) {
    state.isSyncing = false
    state.error = err
  },

  [types.UPDATE_LAST_SYNC] (state) {
    state.isUpdatingLastSync = true
    state.error = null
  },

  [types.UPDATE_LAST_SYNC_SUCCESS] (state) {
    state.isUpdatingLastSync = false
  },

  [types.UPDATE_LAST_SYNC_ERROR] (state, err) {
    state.isUpdatingLastSync = false
    state.error = err
  }
}
