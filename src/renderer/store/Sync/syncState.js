export default () => ({
  isAuthenticating: false,
  isSyncing: false,
  isUpdatingLastSync: false,

  error: null
})
