import { Store } from 'vuex'
// import { createPersistedState } from 'vuex-electron'

import Auth from './Auth'
import Config from './Config'
import Sync from './Sync'
import AttendanceType from './AttendanceType'
import Agency from './Agency'
import Echelon from './Echelon'
import Rank from './Rank'
import Calendar from './Calendar'
import Workshift from './Workshift'
import Asn from './Asn'
import Fingerprint from './Fingerprint'
import Attendance from './Attendance'

export default new Store({
  modules: {
    Auth,
    Config,
    Sync,
    AttendanceType,
    Agency,
    Echelon,
    Rank,
    Calendar,
    Workshift,
    Asn,
    Fingerprint,
    Attendance
  },
  plugins: [
    // createPersistedState()
  ],
  strict: true
})
