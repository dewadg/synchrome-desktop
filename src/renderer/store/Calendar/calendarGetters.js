import * as types from './calendarTypes'
import { denormalize } from 'normalizr'
import { CALENDAR_LIST_SCHEMA } from './calendarSchema'

export default {
  [types.GET_CALENDAR_DATA]: state => denormalize(state.data.result, CALENDAR_LIST_SCHEMA, state.data.entities)
}
