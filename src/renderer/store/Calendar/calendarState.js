export default () => ({
  isFetchingAll: false,

  error: null,

  data: {
    entities: {},
    result: []
  }
})
