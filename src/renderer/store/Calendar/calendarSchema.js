import { schema } from 'normalizr'

export const CALENDAR_EVENT_SCHEMA = new schema.Entity('calendarEvents')
export const CALENDAR_EVENT_LIST_SCHEMA = [CALENDAR_EVENT_SCHEMA]

export const CALENDAR_SCHEMA = new schema.Entity('calendars', {
  events: CALENDAR_EVENT_LIST_SCHEMA
})
export const CALENDAR_LIST_SCHEMA = [CALENDAR_SCHEMA]
