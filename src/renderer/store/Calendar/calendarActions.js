import * as types from './calendarTypes'
import { calendarService } from '~/services'

export default {
  async [types.FETCH_ALL_CALENDARS] ({ commit }) {
    commit(types.FETCH_ALL_CALENDARS)

    try {
      const data = (await calendarService.get()).map(({ dataValues }) => ({
        ...dataValues,
        events: dataValues.events
          ? dataValues.events.map(eventItem => ({ ...eventItem.dataValues }))
          : []
      }))

      commit(types.FETCH_ALL_CALENDARS_SUCCESS, data)
    } catch (err) {
      commit(types.FETCH_ALL_CALENDARS_ERROR, err)
    }
  }
}
