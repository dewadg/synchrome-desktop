import * as types from './calendarTypes'
import { normalize } from 'normalizr'
import { CALENDAR_LIST_SCHEMA } from './calendarSchema'

export default {
  [types.FETCH_ALL_CALENDARS] (state) {
    state.isFetchingAll = true
    state.error = null
  },

  [types.FETCH_ALL_CALENDARS_SUCCESS] (state, data) {
    state.isFetchingAll = false
    state.data = normalize(data, CALENDAR_LIST_SCHEMA)
  },

  [types.FETCH_ALL_CALENDARS_ERROR] (state, err) {
    state.isFetchingAll = false
    state.error = err
  }
}
