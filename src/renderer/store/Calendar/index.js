import state from './calendarState'
import getters from './calendarGetters'
import mutations from './calendarMutations'
import actions from './calendarActions'

export default {
  state,
  getters,
  mutations,
  actions
}
