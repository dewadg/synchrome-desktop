import * as types from './rankTypes'
import { denormalize } from 'normalizr'
import { RANK_LIST_SCHEMA } from './rankSchema'

export default {
  [types.GET_RANK_DATA]: state => denormalize(state.data.result, RANK_LIST_SCHEMA, state.data.entities)
}
