import * as types from './rankTypes'
import { rankService } from '~/services'

export default {
  async [types.FETCH_ALL_RANKS] ({ commit }) {
    commit(types.FETCH_ALL_RANKS)

    try {
      const data = (await rankService.get()).map(item => ({
        ...item.dataValues
      }))

      commit(types.FETCH_ALL_RANKS_SUCCESS, data)
    } catch (err) {
      commit(types.FETCH_ALL_RANKS_ERROR, err)
    }
  }
}
