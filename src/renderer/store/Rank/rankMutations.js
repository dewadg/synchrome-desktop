import * as types from './rankTypes'
import { normalize } from 'normalizr'
import { RANK_LIST_SCHEMA } from './rankSchema'

export default {
  [types.FETCH_ALL_RANKS] (state) {
    state.isFetching = true
    state.error = null
  },

  [types.FETCH_ALL_RANKS_SUCCESS] (state, data) {
    state.isFetching = false
    state.data = normalize(data, RANK_LIST_SCHEMA)
  },

  [types.FETCH_ALL_RANKS_ERROR] (state, err) {
    state.isFetching = false
    state.error = err
  }
}
