import state from './rankState'
import getters from './rankGetters'
import mutations from './rankMutations'
import actions from './rankActions'

export default {
  state,
  getters,
  mutations,
  actions
}
