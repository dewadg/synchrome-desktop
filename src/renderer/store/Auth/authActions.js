import * as types from './authTypes'
import { authService } from '~/services'
import { setItem } from '@/helpers/localStorage'

export default {
  async [types.AUTHENTICATE] ({ commit }, { name, password }) {
    commit(types.AUTHENTICATE)

    try {
      const user = await authService.authenticate(name, password)

      setItem('user', JSON.stringify(user))

      commit(types.AUTHENTICATE_SUCCESS, user)

      return user
    } catch (err) {
      commit(types.AUTHENTICATE_ERROR, err)
    }
  },

  async [types.LOGOUT] ({ commit }) {
    commit(types.LOGOUT)
    setItem('user', '')
    commit(types.LOGOUT_SUCCESS)
  }
}
