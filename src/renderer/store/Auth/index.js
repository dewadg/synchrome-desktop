import state from './authState'
import mutations from './authMutations'
import actions from './authActions'

export default {
  state,
  mutations,
  actions
}
