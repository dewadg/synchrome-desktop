import * as types from './authTypes'

export default {
  [types.AUTHENTICATE] (state) {
    state.isAuthenticating = true
    state.error = null
  },

  [types.AUTHENTICATE_SUCCESS] (state, data) {
    state.isAuthenticating = false
    state.data = {
      id: data.id,
      name: data.name,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt
    }
  },

  [types.AUTHENTICATE_ERROR] (state, err) {
    state.isAuthenticating = false
    state.error = err
  },

  [types.LOGOUT] (state) {
    state.isLoggingOut = true
    state.data = {
      id: '',
      name: '',
      createdAt: '',
      updatedAt: ''
    }
  },

  [types.LOGOUT_SUCCESS] (state) {
    state.isLoggingOut = false
  },

  [types.LOGOUT_ERROR] (state, err) {
    state.isLoggingOut = false
    state.error = err
  }
}
