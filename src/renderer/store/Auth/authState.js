export default () => ({
  isAuthenticating: false,
  isLoggingOut: false,

  error: null,

  data: {
    id: 0,
    name: '',
    createdAt: '',
    updatedAt: ''
  }
})
