import state from './fingerprintState'
import mutations from './fingerprintMutations'
import actions from './fingerprintActions'

export default {
  state,
  mutations,
  actions
}
