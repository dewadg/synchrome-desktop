import * as types from './fingerprintTypes'

export default {
  [types.PREPARE_FINGERPRINT_REGISTRATION] (state) {
    state.isPreparingFingerprintRegistration = true
    state.error = null
  },

  [types.PREPARE_FINGERPRINT_REGISTRATION_SUCCESS] (state, data) {
    state.isPreparingFingerprintRegistration = false
    state.preparationUser = {
      ...state.preparationUser,
      pin: data.pin,
      nama: data.nama
    }
  },

  [types.PREPARE_FINGERPRINT_REGISTRATION_ERROR] (state, err) {
    state.isPreparingFingerprintRegistration = false
    state.error = err
  },

  [types.CLEAR_FINGERPRINT_REGISTRATION] (state) {
    state.isClearingFingerprintRegistration = true
    state.error = null
  },

  [types.CLEAR_FINGERPRINT_REGISTRATION_SUCCESS] (state) {
    state.preparationUser = {
      pin: '',
      nama: ''
    }
    state.isClearingFingerprintRegistration = false
  },

  [types.CLEAR_FINGERPRINT_REGISTRATION_ERROR] (state, err) {
    state.isClearingFingerprintRegistration = false
    state.error = err
  },

  [types.FIND_USER_FROM_FINGERPRINT_MACHINE] (state) {
    state.isFinding = true
    state.error = null
  },

  [types.FIND_USER_FROM_FINGERPRINT_MACHINE_SUCCESS] (state) {
    state.isFinding = false
  },

  [types.FIND_USER_FROM_FINGERPRINT_MACHINE_ERROR] (state, err) {
    state.isFinding = false
    state.error = err
  },

  [types.REGISTER_FINGERPRINT_TO_SERVER] (state) {
    state.isRegistering = false
    state.error = null
  },

  [types.REGISTER_FINGERPRINT_TO_SERVER_SUCCESS] (state) {
    state.isRegistering = false
  },

  [types.REGISTER_FINGERPRINT_TO_SERVER_ERROR] (state, err) {
    state.isRegistering = false
    state.error = err
  },

  [types.SYNC_FINGERPRINTS_TO_MACHINE] (state) {
    state.isSyncing = true
    state.error = null
  },

  [types.SYNC_FINGERPRINTS_TO_MACHINE_SUCCESS] (state) {
    state.isSyncing = false
  },

  [types.SYNC_FINGERPRINTS_TO_MACHINE_ERROR] (state, err) {
    state.isSyncing = false
    state.error = err
  }
}
