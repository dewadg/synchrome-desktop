import * as types from './fingerprintTypes'
import { fingerprintService } from '~/services'

export default {
  async [types.PREPARE_FINGERPRINT_REGISTRATION] ({ commit }) {
    commit(types.PREPARE_FINGERPRINT_REGISTRATION)

    try {
      const data = await fingerprintService.prepareUserForRegisteringFingerprint()

      commit(types.PREPARE_FINGERPRINT_REGISTRATION_SUCCESS, data)
    } catch (err) {
      commit(types.PREPARE_FINGERPRINT_REGISTRATION_ERROR, err)
    }
  },

  async [types.CLEAR_FINGERPRINT_REGISTRATION] ({ commit, state }) {
    commit(types.CLEAR_FINGERPRINT_REGISTRATION)

    try {
      await fingerprintService.deleteUser(state.preparationUser.pin)

      commit(types.CLEAR_FINGERPRINT_REGISTRATION_SUCCESS)
    } catch (err) {
      commit(types.CLEAR_FINGERPRINT_REGISTRATION_ERROR, err)
    }
  },

  async [types.FIND_USER_FROM_FINGERPRINT_MACHINE] ({ commit }, pin) {
    commit(types.FIND_USER_FROM_FINGERPRINT_MACHINE)

    try {
      const data = await fingerprintService.findUser(pin)

      commit(types.FIND_USER_FROM_FINGERPRINT_MACHINE_SUCCESS)

      return data
    } catch (err) {
      commit(types.FIND_USER_FROM_FINGERPRINT_MACHINE_ERROR, err)
    }
  },

  async [types.REGISTER_FINGERPRINT_TO_SERVER] ({ commit }, { asnId, fingerprint }) {
    commit(types.REGISTER_FINGERPRINT_TO_SERVER)

    try {
      const data = await fingerprintService.registerFingerprintToServer(asnId, fingerprint)

      commit(types.REGISTER_FINGERPRINT_TO_SERVER_SUCCESS)

      return data
    } catch (err) {
      commit(types.REGISTER_FINGERPRINT_TO_SERVER_ERROR, err)
    }
  },

  async [types.SYNC_FINGERPRINTS_TO_MACHINE] ({ commit }, payload) {
    commit(types.SYNC_FINGERPRINTS_TO_MACHINE)

    try {
      for (let item of payload) {
        await fingerprintService.registerUser(item)
      }

      commit(types.SYNC_FINGERPRINTS_TO_MACHINE_SUCCESS)
    } catch (err) {
      commit(types.SYNC_FINGERPRINTS_TO_MACHINE_ERROR, err)
    }
  }
}
