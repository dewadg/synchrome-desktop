export default {
  isPreparingFingerprintRegistration: false,
  isClearingFingerprintRegistration: false,
  isFinding: false,
  isRegistering: false,
  isSyncing: false,

  error: null,

  preparationUser: {
    pin: '',
    nama: ''
  }
}
