import * as types from './configTypes'
import { configService } from '~/services'

export default {
  async [types.FETCH_ONE_CONFIG] ({ commit }, key) {
    commit(types.FETCH_ONE_CONFIG)

    try {
      const config = await configService.find(key)
      commit(types.FETCH_ONE_CONFIG_SUCCESS)

      return config
    } catch (err) {
      commit(types.FETCH_ONE_CONFIG_ERROR, err)
    }
  },

  async [types.SET_CONFIG] ({ commit }, { key, value }) {
    commit(types.SET_CONFIG)

    try {
      await configService.set(key, value)

      commit(types.SET_CONFIG_SUCCESS)
    } catch (err) {
      commit(types.SET_CONFIG_ERROR, err)
    }
  }
}
