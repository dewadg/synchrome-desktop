import state from './configState'
import mutations from './configMutations'
import actions from './configActions'

export default {
  state,
  mutations,
  actions
}
