import * as types from './configTypes'

export default {
  [types.FETCH_ONE_CONFIG] (state) {
    state.isFetchingOne = true
    state.error = null
  },

  [types.FETCH_ONE_CONFIG_SUCCESS] (state) {
    state.isFetchingOne = false
  },

  [types.FETCH_ONE_CONFIG_ERROR] (state, err) {
    state.isFetchingOne = false
    state.error = err
  },

  [types.SET_CONFIG] (state) {
    state.isSetting = true
    state.error = null
  },

  [types.SET_CONFIG_SUCCESS] (state) {
    state.isSetting = false
  },

  [types.SET_CONFIG_ERROR] (state, err) {
    state.isSetting = false
    state.error = err
  }
}
