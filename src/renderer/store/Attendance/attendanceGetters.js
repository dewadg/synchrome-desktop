import { denormalize } from 'normalizr'
import * as types from './attendanceTypes'
import { ATTENDANCE_LIST_SCHEMA } from './attendanceSchema'

export default {
  [types.GET_ATTENDANCE_DATA]: state => denormalize(state.data.result, ATTENDANCE_LIST_SCHEMA, state.data.entities)
}
