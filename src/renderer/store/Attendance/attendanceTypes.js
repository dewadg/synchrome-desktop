export const FETCH_ALL_ATTENDANCES = 'FETCH_ALL_ATTENDANCES'
export const FETCH_ALL_ATTENDANCES_SUCCESS = 'FETCH_ALL_ATTENDANCES_SUCCESS'
export const FETCH_ALL_ATTENDANCES_ERROR = 'FETCH_ALL_ATTENDANCES_ERROR'

export const GENERATE_ATTENDANCES = 'GENERATE_ATTENDANCES'
export const GENERATE_ATTENDANCES_SUCCESS = 'GENERATE_ATTENDANCES_SUCCESS'
export const GENERATE_ATTENDANCES_ERROR = 'GENERATE_ATTENDANCES_ERROR'

export const FETCH_ATTENDANCES_FROM_MACHINE = 'FETCH_ATTENDANCES_FROM_MACHINE'
export const FETCH_ATTENDANCES_FROM_MACHINE_SUCCESS = 'FETCH_ATTENDANCES_FROM_MACHINE_SUCCESS'
export const FETCH_ATTENDANCES_FROM_MACHINE_ERROR = 'FETCH_ATTENDANCES_FROM_MACHINE_ERROR'

export const PROCESS_ATTENDANCES = 'PROCESS_ATTENDANCES'
export const PROCESS_ATTENDANCES_SUCCESS = 'PROCESS_ATTENDANCES_SUCCESS'
export const PROCESS_ATTENDANCES_ERROR = 'PROCESS_ATTENDANCES_ERROR'

export const GET_ATTENDANCE_DATA = 'GET_ATTENDANCE_DATA'
export const SET_ATTENDANCE_DATA = 'SET_ATTENDANCE_DATA'
