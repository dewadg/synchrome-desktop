export default {
  isFetching: false,
  isGenerating: false,
  isFetchingFromMachine: false,
  isProcessing: false,

  error: null,

  data: {
    entities: {},
    result: []
  }
}
