import { schema } from 'normalizr'
import { ASN_SCHEMA } from '../Asn/asnSchema'
import { ATTENDANCE_TYPE_SCHEMA } from '../AttendanceType/attendanceTypeSchema'

export const ATTENDANCE_SCHEMA = new schema.Entity('attendances', {
  asn: ASN_SCHEMA,
  type: ATTENDANCE_TYPE_SCHEMA
})
export const ATTENDANCE_LIST_SCHEMA = [ATTENDANCE_SCHEMA]
