import state from './attendanceState'
import getters from './attendanceGetters'
import mutations from './attendanceMutations'
import actions from './attendanceActions'

export default {
  state,
  getters,
  mutations,
  actions
}
