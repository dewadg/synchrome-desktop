import * as types from './attendanceTypes'
import { attendanceService } from '~/services'

export default {
  async [types.FETCH_ALL_ATTENDANCES] ({ commit }, date) {
    commit(types.FETCH_ALL_ATTENDANCES)

    try {
      const data = await attendanceService.get(date)

      commit(types.SET_ATTENDANCE_DATA, data)
      commit(types.FETCH_ALL_ATTENDANCES_SUCCESS)
    } catch (err) {
      commit(types.FETCH_ALL_ATTENDANCES_ERROR, err)
    }
  },

  async [types.GENERATE_ATTENDANCES] ({ commit }, date) {
    commit(types.GENERATE_ATTENDANCES)

    try {
      await attendanceService.generate(date)
      const data = await attendanceService.get(date)

      commit(types.SET_ATTENDANCE_DATA, data)
      commit(types.GENERATE_ATTENDANCES_SUCCESS)
    } catch (err) {
      commit(types.GENERATE_ATTENDANCES_ERROR, err)
    }
  },

  async [types.PROCESS_ATTENDANCES] ({ commit }, date) {
    let data

    try {
      commit(types.FETCH_ATTENDANCES_FROM_MACHINE)
      data = await attendanceService.fetchFromMachine(date)
      commit(types.FETCH_ATTENDANCES_FROM_MACHINE_SUCCESS)
    } catch (err) {
      console.log(err)
      commit(types.FETCH_ATTENDANCES_FROM_MACHINE_ERROR, err)
    }

    try {
      commit(types.PROCESS_ATTENDANCES)
      await attendanceService.process(date, data)
      commit(types.PROCESS_ATTENDANCES_SUCCESS)
    } catch (err) {
      console.log(err)
      commit(types.PROCESS_ATTENDANCES_ERROR, err)
    }
  }
}
