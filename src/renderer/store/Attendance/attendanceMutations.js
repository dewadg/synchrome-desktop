import { normalize } from 'normalizr'
import * as types from './attendanceTypes'
import { ATTENDANCE_LIST_SCHEMA } from './attendanceSchema'

export default {
  [types.FETCH_ALL_ATTENDANCES] (state) {
    state.isFetching = true
    state.error = null
  },

  [types.FETCH_ALL_ATTENDANCES_SUCCESS] (state) {
    state.isFetching = false
  },

  [types.FETCH_ALL_ATTENDANCES_ERROR] (state, err) {
    state.isFetching = false
    state.error = err
  },

  [types.GENERATE_ATTENDANCES] (state) {
    state.isGenerating = true
    state.error = null
  },

  [types.GENERATE_ATTENDANCES_SUCCESS] (state) {
    state.isGenerating = false
  },

  [types.GENERATE_ATTENDANCES_ERROR] (state, err) {
    state.isGenerating = false
    state.error = err
  },

  [types.PROCESS_ATTENDANCES] (state) {
    state.isProcessing = true
    state.error = null
  },

  [types.PROCESS_ATTENDANCES_SUCCESS] (state) {
    state.isProcessing = false
  },

  [types.PROCESS_ATTENDANCES_ERROR] (state, err) {
    state.isProcessing = false
    state.error = err
  },

  [types.FETCH_ATTENDANCES_FROM_MACHINE] (state) {
    state.isFetchingFromMachine = true
    state.error = null
  },

  [types.FETCH_ATTENDANCES_FROM_MACHINE_SUCCESS] (state) {
    state.isFetchingFromMachine = false
  },

  [types.FETCH_ATTENDANCES_FROM_MACHINE_ERROR] (state, err) {
    state.isFetchingFromMachine = false
    state.error = err
  },

  [types.SET_ATTENDANCE_DATA] (state, data) {
    state.data = normalize(data, ATTENDANCE_LIST_SCHEMA)
  }
}
