import * as types from './asnTypes'
import { denormalize } from 'normalizr'
import { ASN_LIST_SCHEMA } from './asnSchema'

export default {
  [types.GET_ASN_DATA]: state => denormalize(state.data.result, ASN_LIST_SCHEMA, state.data.entities)
}
