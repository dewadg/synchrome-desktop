export default () => ({
  isFetchingAll: false,
  isFetchingOne: false,

  error: null,

  data: {
    entities: {},
    result: []
  },

  singleData: {
    id: '',
    name: '',
    pin: '',
    phone: '',
    address: '',
    agency: {
      id: '',
      name: ''
    },
    rank: {
      id: '',
      name: ''
    },
    echelon: {
      id: '',
      name: ''
    },
    tpp: {
      id: 0,
      name: '',
      value: 0
    },
    workshift: {
      id: 0,
      name: ''
    },
    calendar: {
      id: 0,
      name: ''
    },
    fingerprints: []
  }
})
