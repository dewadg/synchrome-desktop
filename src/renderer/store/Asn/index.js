import state from './asnState'
import getters from './asnGetters'
import mutations from './asnMutations'
import actions from './asnActions'

export default {
  state,
  getters,
  mutations,
  actions
}
