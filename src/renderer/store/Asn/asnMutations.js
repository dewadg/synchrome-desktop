import * as types from './asnTypes'
import { normalize } from 'normalizr'
import { ASN_LIST_SCHEMA } from './asnSchema'

export default {
  [types.FETCH_ALL_ASN] (state) {
    state.isFetchingAll = true
    state.error = null
  },

  [types.FETCH_ALL_ASN_SUCCESS] (state, data) {
    state.isFetchingAll = false
    state.data = normalize(data, ASN_LIST_SCHEMA)
  },

  [types.FETCH_ALL_ASN_ERROR] (state, err) {
    state.isFetchingAll = false
    state.error = err
  },

  [types.FETCH_ONE_ASN] (state) {
    state.isFetchingOne = true
    state.error = null
  },

  [types.FETCH_ONE_ASN_SUCCESS] (state, data) {
    state.isFetchingOne = false
    state.singleData = {
      ...state.singleData,
      ...data
    }
  },

  [types.FETCH_ONE_ASN_ERROR] (state, err) {
    state.isFetchingOne = false
    state.error = err
  }
}
