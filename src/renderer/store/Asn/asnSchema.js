import { schema } from 'normalizr'
import { AGENCY_SCHEMA } from '../Agency/agencySchema'
import { RANK_SCHEMA } from '../Rank/rankSchema'
import { ECHELON_SCHEMA } from '../Echelon/echelonSchema'
import { TPP_SCHEMA } from '../Tpp/tppSchema'
import { WORKSHIFT_SCHEMA } from '../Workshift/workshiftSchema'
import { CALENDAR_SCHEMA } from '../Calendar/calendarSchema'

export const ASN_SCHEMA = new schema.Entity('asns', {
  agency: AGENCY_SCHEMA,
  rank: RANK_SCHEMA,
  echelon: ECHELON_SCHEMA,
  tpp: TPP_SCHEMA,
  workshift: WORKSHIFT_SCHEMA,
  calendar: CALENDAR_SCHEMA
})
export const ASN_LIST_SCHEMA = [ASN_SCHEMA]
