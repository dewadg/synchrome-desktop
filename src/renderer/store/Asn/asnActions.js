import * as types from './asnTypes'
import { asnService } from '~/services'

export default {
  async [types.FETCH_ALL_ASN] ({ commit }) {
    commit(types.FETCH_ALL_ASN)

    try {
      const data = (await asnService.get()).map(({ dataValues }) => ({
        ...dataValues,
        agency: dataValues.agency
          ? { ...dataValues.agency.dataValues }
          : null,
        rank: dataValues.rank
          ? { ...dataValues.rank.dataValues }
          : null,
        echelon: dataValues.echelon
          ? { ...dataValues.echelon.dataValues }
          : null,
        tpp: dataValues.tpp
          ? { ...dataValues.tpp.dataValues }
          : null,
        workshift: dataValues.workshift
          ? { ...dataValues.workshift.dataValues }
          : null,
        calendar: dataValues.calendar
          ? { ...dataValues.calendar.dataValues }
          : null,
        fingerprints: dataValues.fingerprints
          ? dataValues.fingerprints.map(item => ({
            ...item.dataValues
          }))
          : []
      }))

      commit(types.FETCH_ALL_ASN_SUCCESS, data)
    } catch (err) {
      commit(types.FETCH_ALL_ASN_ERROR, err)
    }
  },

  async [types.FETCH_ONE_ASN] ({ commit }, id) {
    commit(types.FETCH_ONE_ASN)

    try {
      const data = (await asnService.find(id)).dataValues

      commit(types.FETCH_ONE_ASN_SUCCESS, {
        ...data,
        agency: data.agency
          ? { ...data.agency.dataValues }
          : null,
        calendar: data.calendar
          ? { ...data.calendar.dataValues }
          : null,
        echelon: data.echelon
          ? { ...data.echelon.dataValues }
          : null,
        rank: data.rank
          ? { ...data.rank.dataValues }
          : null,
        workshift: data.workshift
          ? { ...data.workshift.dataValues }
          : null,
        tpp: data.tpp
          ? { ...data.tpp.dataValues }
          : null,
        fingerprints: data.fingerprints
          ? data.fingerprints.map(item => ({
            ...item.dataValues
          }))
          : []
      })
    } catch (err) {
      commit(types.FETCH_ONE_ASN_ERROR, err)
    }
  }
}
