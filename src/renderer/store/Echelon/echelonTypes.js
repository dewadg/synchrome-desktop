export const FETCH_ALL_ECHELONS = 'FETCH_ALL_ECHELONS'
export const FETCH_ALL_ECHELONS_SUCCESS = 'FETCH_ALL_ECHELONS_SUCCESS'
export const FETCH_ALL_ECHELONS_ERROR = 'FETCH_ALL_ECHELONS_ERROR'

export const GET_ECHELON_DATA = 'GET_ECHELON_DATA'
