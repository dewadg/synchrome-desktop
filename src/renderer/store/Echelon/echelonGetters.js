import * as types from './echelonTypes'
import { denormalize } from 'normalizr'
import { ECHELON_LIST_SCHEMA } from './echelonSchema'

export default {
  [types.GET_ECHELON_DATA]: state => denormalize(state.data.result, ECHELON_LIST_SCHEMA, state.data.entities)
}
