import state from './echelonState'
import getters from './echelonGetters'
import mutations from './echelonMutations'
import actions from './echelonActions'

export default {
  state,
  getters,
  mutations,
  actions
}
