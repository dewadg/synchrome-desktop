import * as types from './echelonTypes'
import { normalize } from 'normalizr'
import { ECHELON_LIST_SCHEMA } from './echelonSchema'

export default {
  [types.FETCH_ALL_ECHELONS] (state) {
    state.isFetching = true
    state.error = null
  },

  [types.FETCH_ALL_ECHELONS_SUCCESS] (state, data) {
    state.isFetching = false
    state.data = normalize(data, ECHELON_LIST_SCHEMA)
  },

  [types.FETCH_ALL_ECHELONS_ERROR] (state, err) {
    state.isFetching = false
    state.error = err
  }
}
