import * as types from './echelonTypes'
import { echelonService } from '~/services'

export default {
  async [types.FETCH_ALL_ECHELONS] ({ commit }) {
    commit(types.FETCH_ALL_ECHELONS)

    try {
      const data = (await echelonService.get()).map(({ dataValues }) => ({
        ...dataValues,
        type: dataValues.type
          ? { ...dataValues.type.dataValues }
          : null,
        supervisor: dataValues.supervisor
          ? { ...dataValues.supervisor.dataValues }
          : null
      }))

      commit(types.FETCH_ALL_ECHELONS_SUCCESS, data)
    } catch (err) {
      commit(types.FETCH_ALL_ECHELONS_ERROR, err)
    }
  }
}
