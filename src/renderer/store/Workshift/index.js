import state from './workshiftState'
import getters from './workshiftGetters'
import mutations from './workshiftMutations'
import actions from './workshiftActions'

export default {
  state,
  getters,
  mutations,
  actions
}
