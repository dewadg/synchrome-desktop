import * as types from './workshiftTypes'
import { normalize } from 'normalizr'
import { WORKSHIFT_LIST_SCHEMA } from './workshiftSchema'

export default {
  [types.FETCH_ALL_WORKSHIFTS] (state) {
    state.isFetchingAll = true
    state.error = null
  },

  [types.FETCH_ALL_WORKSHIFTS_SUCCESS] (state, data) {
    state.isFetchingAll = false
    state.data = normalize(data, WORKSHIFT_LIST_SCHEMA)
  },

  [types.FETCH_ALL_WORKSHIFTS_ERROR] (state, err) {
    state.isFetchingAll = false
    state.error = err
  }
}
