import { schema } from 'normalizr'

export const WORKSHIFT_DETAILS_SCHEMA = new schema.Entity('workshiftDetails')
export const WORKSHIFT_DETAILS_LIST_SCHEMA = [WORKSHIFT_DETAILS_SCHEMA]

export const WORKSHIFT_SCHEMA = new schema.Entity('workshifts', {
  details: WORKSHIFT_DETAILS_LIST_SCHEMA
})
export const WORKSHIFT_LIST_SCHEMA = [WORKSHIFT_SCHEMA]
