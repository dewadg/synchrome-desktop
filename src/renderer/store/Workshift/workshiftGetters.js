import * as types from './workshiftTypes'
import { denormalize } from 'normalizr'
import { WORKSHIFT_LIST_SCHEMA } from './workshiftSchema'

export default {
  [types.GET_WORKSHIFT_DATA]: state => denormalize(state.data.result, WORKSHIFT_LIST_SCHEMA, state.data.entities)
}
