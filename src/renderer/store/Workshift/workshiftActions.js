import * as types from './workshiftTypes'
import { workshiftService } from '~/services'

export default {
  async [types.FETCH_ALL_WORKSHIFTS] ({ commit }) {
    commit(types.FETCH_ALL_WORKSHIFTS)

    try {
      const data = (await workshiftService.get()).map(({ dataValues }) => ({
        ...dataValues,
        details: dataValues.details
          ? dataValues.details.map(detailsItem => ({ ...detailsItem.dataValues }))
          : []
      }))

      commit(types.FETCH_ALL_WORKSHIFTS_SUCCESS, data)
    } catch (err) {
      commit(types.FETCH_ALL_WORKSHIFTS_ERROR, err)
    }
  }
}
