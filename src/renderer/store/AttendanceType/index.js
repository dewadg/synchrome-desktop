import state from './attendanceTypeState'
import getters from './attendanceTypeGetters'
import mutations from './attendanceTypeMutations'
import actions from './attendanceTypeActions'

export default {
  state,
  getters,
  mutations,
  actions
}
