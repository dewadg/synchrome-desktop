import * as types from './attendanceTypeTypes'
import { denormalize } from 'normalizr'
import { ATTENDANCE_TYPE_LIST_SCHEMA } from './attendanceTypeSchema'

export default {
  [types.GET_ATTENDANCE_TYPE_DATA]: state => denormalize(state.data.result, ATTENDANCE_TYPE_LIST_SCHEMA, state.data.entities)
}
