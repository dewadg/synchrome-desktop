import * as types from './attendanceTypeTypes'
import { attendanceTypeService } from '~/services'

export default {
  async [types.FETCH_ALL_ATTENDANCE_TYPES] ({ commit }) {
    commit(types.FETCH_ALL_ATTENDANCE_TYPES)

    try {
      const data = await attendanceTypeService.get()

      commit(types.FETCH_ALL_ATTENDANCE_TYPES_SUCCESS, data)
    } catch (err) {
      commit(types.FETCH_ALL_ATTENDANCE_TYPES_ERROR, err)
    }
  }
}
