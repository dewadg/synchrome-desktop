import * as types from './attendanceTypeTypes'
import { normalize } from 'normalizr'
import { ATTENDANCE_TYPE_LIST_SCHEMA } from './attendanceTypeSchema'

export default {
  [types.FETCH_ALL_ATTENDANCE_TYPES] (state) {
    state.isFetching = true
    state.error = null
  },

  [types.FETCH_ALL_ATTENDANCE_TYPES_SUCCESS] (state, data) {
    state.isFetching = false
    state.data = normalize(data, ATTENDANCE_TYPE_LIST_SCHEMA)
  },

  [types.FETCH_ALL_ATTENDANCE_TYPES_ERROR] (state, err) {
    state.isFetching = false
    state.error = err
  }
}
