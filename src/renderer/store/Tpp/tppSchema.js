import { schema } from 'normalizr'

export const TPP_SCHEMA = new schema.Entity('tpps')
export const TPP_LIST_SCHEMA = [TPP_SCHEMA]
