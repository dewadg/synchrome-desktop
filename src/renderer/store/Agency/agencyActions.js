import * as types from './agencyTypes'
import { agencyService } from '~/services'

export default {
  async [types.FETCH_ALL_AGENCIES] ({ commit }) {
    commit(types.FETCH_ALL_AGENCIES)

    try {
      const data = (await agencyService.get()).map(item => ({
        ...item.dataValues
      }))

      commit(types.FETCH_ALL_AGENCIES_SUCCESS, data)
    } catch (err) {
      commit(types.FETCH_ALL_AGENCIES_ERROR, err)
    }
  }
}
