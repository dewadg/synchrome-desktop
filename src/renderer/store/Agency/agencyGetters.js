import * as types from './agencyTypes'
import { denormalize } from 'normalizr'
import { AGENCY_LIST_SCHEMA } from './agencySchema'

export default {
  [types.GET_AGENCY_DATA]: state => denormalize(state.data.result, AGENCY_LIST_SCHEMA, state.data.entities)
}
