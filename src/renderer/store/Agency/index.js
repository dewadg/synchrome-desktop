import state from './agencyState'
import getters from './agencyGetters'
import mutations from './agencyMutations'
import actions from './agencyActions'

export default {
  state,
  getters,
  mutations,
  actions
}
