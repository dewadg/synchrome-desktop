export default [
  {
    text: 'Dashboard',
    to: {
      name: 'dashboard'
    },
    exact: true
  },
  {
    text: 'ASN',
    to: {
      name: 'asn.index'
    },
    exact: true
  },
  {
    text: 'Sidik Jari',
    disabled: true
  }
]
