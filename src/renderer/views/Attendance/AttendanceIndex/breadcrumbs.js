export default [
  {
    text: 'Dashboard',
    to: {
      name: 'dashboard'
    },
    exact: true
  },
  {
    text: 'Presensi',
    disabled: true
  }
]
