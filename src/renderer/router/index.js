import Router from 'vue-router'

import Login from '@/views/Login'
import Dashboard from '@/views/Dashboard'
import Sync from '@/views/Sync'

import AgencyIndex from '@/views/Agency/AgencyIndex'
import EchelonIndex from '@/views/Echelon/EchelonIndex'
import RankIndex from '@/views/Rank/RankIndex'
import CalendarIndex from '@/views/Calendar/CalendarIndex'
import WorkshiftIndex from '@/views/Workshift/WorkshiftIndex'
import AsnIndex from '@/views/Asn/AsnIndex'
import AsnFingerprint from '@/views/Asn/AsnFingerprint'
import AttendanceIndex from '@/views/Attendance/AttendanceIndex'

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/sync',
      name: 'sync',
      component: Sync
    },
    {
      path: '/agencies',
      name: 'agencies.index',
      component: AgencyIndex
    },
    {
      path: '/echelons',
      name: 'echelons.index',
      component: EchelonIndex
    },
    {
      path: '/ranks',
      name: 'ranks.index',
      component: RankIndex
    },
    {
      path: '/calendars',
      name: 'calendars.index',
      component: CalendarIndex
    },
    {
      path: '/workshifts',
      name: 'workshifts.index',
      component: WorkshiftIndex
    },
    {
      path: '/asn',
      name: 'asn.index',
      component: AsnIndex
    },
    {
      path: 'asn/:id/fingerprints',
      name: 'asn.fingerprints',
      component: AsnFingerprint
    },
    {
      path: 'attendance',
      name: 'attendance.index',
      component: AttendanceIndex
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
