export default {
  host: process.env.ELECTRON_WEBPACK_APP_EASYLINK_URL || 'http://localhost:7005',
  serialNumber: process.env.ELECTRON_WEBPACK_EASYLINK_SERIAL_NUMBER || ''
}
