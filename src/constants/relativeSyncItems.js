export default [
  {
    target: 'agencies/{id}/asn',
    model: 'Asn',
    text: 'Aparatur Sipil Negara'
  },
  {
    target: 'attendances',
    model: 'Attendance',
    text: 'Data Kehadiran'
  }
]
