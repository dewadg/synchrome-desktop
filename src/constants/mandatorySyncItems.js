export default [
  {
    target: 'attendance-types',
    model: 'AttendanceType',
    text: 'Jenis Presensi'
  },
  {
    target: 'echelon-types',
    model: 'EchelonType',
    text: 'Jenis Eselon'
  },
  {
    target: 'echelons',
    model: 'Echelon',
    text: 'Eselon'
  },
  {
    target: 'ranks',
    model: 'Rank',
    text: 'Golongan'
  },
  {
    target: 'agencies',
    model: 'Agency',
    text: 'Organisasi Perangkat Daerah'
  },
  {
    target: 'calendars',
    model: 'Calendar',
    text: 'Kalender'
  },
  {
    target: 'workshifts',
    model: 'Workshift',
    text: 'Shift Kerja'
  },
  {
    target: 'tpp',
    model: 'Tpp',
    text: 'TPP'
  }
]
