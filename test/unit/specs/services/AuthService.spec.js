const moment = require('moment')
const AuthService = require('../../../../src/services/AuthService')
const bcryptStub = require('../../../stubs/bcrypt')
const userStub = require('../../../stubs/db/models/user')

let service

describe('AuthService', () => {
  beforeEach(() => {
    userStub.sinonReset()
    bcryptStub.sinonReset()

    service = new AuthService({ User: userStub }, bcryptStub)
  })

  it('should authenticate user successfully', async () => {
    const userStubExpectedPayload = {
      where: {
        name: 'admin'
      },
      limit: 1
    }

    const userStubExpectedReturn = {
      id: 1,
      name: 'admin',
      password: 'admin',
      createdAt: moment().format('YYYY-MM-DD'),
      updatedAt: moment().format('YYYY-MM-DD')
    }

    const bcryptStubExpectedPayload = {
      name: 'admin',
      password: 'admin'
    }

    const bcryptStubExpectedReturn = true

    userStub.findAll
      .withArgs(userStubExpectedPayload)
      .returns(userStubExpectedReturn)

    bcryptStub.compareSync
      .withArgs(bcryptStubExpectedPayload.name, bcryptStubExpectedPayload.password)
      .returns(bcryptStubExpectedReturn)

    const actual = await service.authenticate('admin', 'admin')

    expect(actual.name).to.equals('admin')
  })
})
