const sinon = require('sinon')

const user = {
  findAll: sinon.stub(),

  sinonReset () {
    this.findAll.reset()
  }
}

module.exports = user
