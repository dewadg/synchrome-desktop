const sinon = require('sinon')

const bcrypt = {
  compareSync: sinon.stub(),

  sinonReset () {
    this.compareSync.reset()
  }
}

module.exports = bcrypt
